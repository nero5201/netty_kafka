package com.lx.cp.controller;

import com.lx.cp.netty.handler.TcpServerHandler;
import com.lx.cp.utils.MsgUtil;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;

@Slf4j
@RestController
public class TcpClientController {

    @Value("${gps.netty.tcp.port}")
    private int port;

    @PostMapping("sendTcp")
    public String send(String msg){
        EventLoopGroup group = new NioEventLoopGroup();
        Bootstrap b = new Bootstrap();
        Channel ch =null;
        TcpServerHandler handler=new TcpServerHandler();

        try {
            b.group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<NioSocketChannel>() {
                        @Override
                        protected void initChannel(NioSocketChannel ch) throws Exception {
                            ch.pipeline().addLast(
                                    new DelimiterBasedFrameDecoder(1024, Unpooled.copiedBuffer(new byte[] { 0x7e }),
                                            Unpooled.copiedBuffer(new byte[] { 0x7e, 0x7e })));
                            ch.pipeline().addLast(handler);
                        }});
            ch =b.connect("localhost", port).sync().channel();

        } catch (Exception e) {
            e.printStackTrace();
        }

        ByteBuf tcpMsg = Unpooled.copiedBuffer(msg.getBytes(StandardCharsets.UTF_8));

        ByteBuf buf= Unpooled.buffer(msg.length() + 1);
        try{
            log.info("TCP客户端发送消息：{}", msg);
            buf.writeBytes(tcpMsg);//消息体
            buf.writeByte(MsgUtil.DELIMITER);//消息分割符
            ch.writeAndFlush(buf).sync();
        }catch (Exception e){
            log.info("TCP客户端发送消息失败：{}", e);
        }

        //关闭链接
        group.shutdownGracefully();
        return "success";
    }
}
